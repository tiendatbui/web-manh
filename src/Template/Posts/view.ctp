
<div id="fh5co-testimonial">
    <div class="container">
        <div class="row animate-box">
            <div class="col-md-6 col-md-offset-3 text-center fh5co-heading">
                <h2><?php echo $post['title'];?></h2>
            </div>
        </div>

        <?php
            echo $post['content'];
        ?>
    </div>

</div>