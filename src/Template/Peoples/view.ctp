<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\People $people
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit People'), ['action' => 'edit', $people->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete People'), ['action' => 'delete', $people->id], ['confirm' => __('Are you sure you want to delete # {0}?', $people->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Peoples'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New People'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="peoples view large-9 medium-8 columns content">
    <h3><?= h($people->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($people->id) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Content') ?></h4>
        <?= $this->Text->autoParagraph(h($people->content)); ?>
    </div>
</div>
