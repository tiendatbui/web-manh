<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Introductions Controller
 *
 * @property \App\Model\Table\IntroductionsTable $Introductions
 *
 * @method \App\Model\Entity\Introduction[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class IntroductionsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $_SESSION['active'] = 'introduction';
        $introductions = $this->Introductions->find()->order(['id' => 'DESC'])->first();

        $this->set(compact('introductions'));
    }

    /**
     * View method
     *
     * @param string|null $id Introduction id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $introduction = $this->Introductions->get($id, [
            'contain' => []
        ]);

        $this->set('introduction', $introduction);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $introduction = $this->Introductions->newEntity();
        if ($this->request->is('post')) {
            $introduction = $this->Introductions->patchEntity($introduction, $this->request->getData());
            if ($this->Introductions->save($introduction)) {
                $this->Flash->success(__('The introduction has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The introduction could not be saved. Please, try again.'));
        }
        $this->set(compact('introduction'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Introduction id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $introduction = $this->Introductions->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $introduction = $this->Introductions->patchEntity($introduction, $this->request->getData());
            if ($this->Introductions->save($introduction)) {
                $this->Flash->success(__('The introduction has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The introduction could not be saved. Please, try again.'));
        }
        $this->set(compact('introduction'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Introduction id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $introduction = $this->Introductions->get($id);
        if ($this->Introductions->delete($introduction)) {
            $this->Flash->success(__('The introduction has been deleted.'));
        } else {
            $this->Flash->error(__('The introduction could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
